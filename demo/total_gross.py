import csv

mydict = {}

with open('origin.csv', 'r') as file:
	reader = csv.reader(file)
	for row in reader:
		genre = row[5].split(', ')[0].split(' (')[0]
		gross = row[10]
		if genre == 'Genres' or gross == 'Gross':
			pass
		else:
			if genre not in mydict.keys():
				mydict[genre] = int(gross)/1000000
			else:
				mydict[genre] += int(gross)/1000000

for key, value in mydict.items():
	print(key, value)
