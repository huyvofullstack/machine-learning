setwd('/home/huy/Dropbox/Term 4/ML/demo/')

my_data <- read.csv('dataset_350.csv')

library(psych)

pairs.panels(my_data[c('Stars', 'Director', 'Writers', 'Genres', 'Release', 'Runtime', 'Rated', 'Company', 'Gross')])

my_model <- lm(Gross ~ Stars + Director + Writers + Genres + Release + Runtime + Rated + Company, data = my_data)

summary(my_model)

test <- read.csv('test.csv')

my_test <- predict(my_model, test)

my_test
