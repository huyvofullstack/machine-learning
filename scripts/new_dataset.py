import csv
import os
from sys import argv


def open_file():
	actors_list = {}
	directors_list = {}
	writers_list = {}
	company_list = {}
	genres_list = {}
	list = [actors_list, directors_list, writers_list, company_list, genres_list]

	j = 0
	for i in range(2, 7):
		with open(argv[i], 'r') as file:
			mycsv = csv.reader(file)
			for row in mycsv:
				list[j][row[1]] = row[2]
		j += 1
	
	return list[0], list[1], list[2], list[3], list[4]

actors_list, directors_list, writers_list, company_list, genres_list = open_file()

with open(argv[1], 'r') as in_file:
	reader = csv.reader(in_file)

	with open('output_1.csv', 'w') as out_file:
		writer = csv.writer(out_file)
		for row in reader:
			for key, value in actors_list.items():
				if row[2].split(', ')[0] == key:
					row[2] = value
					writer.writerow(row)

with open('output_1.csv', 'r') as in_file:
	reader = csv.reader(in_file)

	with open('output_2.csv', 'w') as out_file:
		writer = csv.writer(out_file)
		for row in reader:
			for key, value in directors_list.items():
				if row[3].split(', ')[0] == key:
					row[3] = value
					writer.writerow(row)

with open('output_2.csv', 'r') as in_file:
	reader = csv.reader(in_file)

	with open('output_3.csv', 'w') as out_file:
		writer = csv.writer(out_file)
		for row in reader:
			for key, value in writers_list.items():
				if row[4].split(', ')[0].split(' (')[0] == key:
					row[4] = value
					writer.writerow(row)

with open('output_3.csv', 'r') as in_file:
	reader = csv.reader(in_file)

	with open('output_4.csv', 'w') as out_file:
		writer = csv.writer(out_file)
		for row in reader:
			for key, value in genres_list.items():
				if row[5].split(', ')[0] == key:
					row[5] = value
					writer.writerow(row)

with open('output_4.csv', 'r') as in_file:
	reader = csv.reader(in_file)

	with open('dataset.csv', 'w') as out_file:
		writer = csv.writer(out_file)
		for row in reader:
			for key, value in company_list.items():
				if row[9].split(', ')[0] == key:
					row[9] = value
					writer.writerow(row)

os.remove('output_1.csv')
os.remove('output_2.csv')
os.remove('output_3.csv')
os.remove('output_4.csv')
