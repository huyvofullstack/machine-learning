import requests
from bs4 import BeautifulSoup

def get_results(name):
	re = requests.get('https://www.google.com/search', params={'q':name})
	soup = BeautifulSoup(re.text, 'lxml')
	response = soup.find('div', {'id': 'resultStats'})
	return int(''.join(filter('0123456789'.__contains__, response.text)))
