import csv
import json
import requests
from sys import argv


def gross_generate(str):
	gross = int(str)
	if gross < 50000000:
		return 1
	elif gross < 100000000:
		return 2
	elif gross < 200000000:
		return 3
	elif gross < 500000000:
		return 4
	else:
		return 5

def rated_generate(rated):
	if rated == 'G':
		return 1
	elif rated == 'PG':
		return 2
	elif rated == 'PG-13':
		return 3
	else:
		return 4

def runtime_generate(minutes):
	mins = int(minutes[0])
	if mins < 90:
		return 1
	elif mins < 120:
		return 2
	elif mins < 150:
		return 3
	elif mins < 180:
		return 4
	else:
		return 5

def release_generate(release):
	if release == ['N/A']:
		return 4
	else:
		if release[1] in ['Jan', 'Feb', 'Mar']:
			return 1
		elif release[1] in ['Apr', 'May', 'Jun']:
			return 3
		elif release[1] in ['Jul', 'Aug', 'Sep']:
			return 4
		else:
			return 2

with open(argv[1], 'w') as dataset:
	fieldnames = [
		' ',
		'Title',
		'Stars', 
		'Director', 
		'Writers', 
		'Genres', 
		'Release', 
		'Runtime',
		'Rated',
		'Production Company',
		'Gross'
	]
	writer = csv.DictWriter(dataset, fieldnames=fieldnames)

	writer.writeheader()

	with open(argv[2]) as file:
		lines = [line.rstrip('\n') for line in file]

		for line in lines:
			string = line.split('::')
			title = string[1].replace(' ', '+').replace('&', '%26')
			year = string[3]

			url = "http://www.omdbapi.com/?t={}&y={}&plot=short&r=json".format(title, year)
			re = requests.get(url)
			response = re.json()

			writer.writerow({
				' ': string[0],
				'Title': string[1],
				'Stars': response['Actors'], 
				'Director': response['Director'],
				'Writers': response['Writer'],
				'Genres': response['Genre'],
				'Release': release_generate(response['Released'].split(' ')),
				'Runtime': runtime_generate(response['Runtime'].split(' ')),
				'Rated': rated_generate(response['Rated']),
				'Gross': gross_generate(string[2].replace(',', ''))
			})
