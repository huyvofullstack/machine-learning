import csv
from sys import argv


def count(name, list):
	count = 0
	for list_item in list:
		if list_item == name:
			count += 1
	return count

def write_csv(filename, final_list):
	with open(filename, 'w') as file:
		fieldnames = [' ', 'Name', 'Count']
		writer = csv.DictWriter(file, fieldnames=fieldnames)
		writer.writeheader()
		ID = 0
		for item in final_list:
			ID += 1
			writer.writerow({' ': ID, 'Name': item[0], 'Count': item[1]})

def make_list_from(filename, row_number):
	mylist = []

	with open(filename, 'r') as file:
		mycsv = csv.reader(file)
		for row in mycsv:
			text = row[row_number].split(', ')
			if len(text) > 1:
				for i in range(2):
					mylist.append(text[i])
			else:
				mylist.append(text[0])

	return mylist

def make_actors_list_from(filename, row_number):
	mylist = []

	with open(filename, 'r') as file:
		mycsv = csv.reader(file)
		for row in mycsv:
			text = row[row_number].split(', ')
			mylist.append(text[0])

	return mylist

def make_writer_list_from(filename, row_number):
	mylist = []

	with open(filename, 'r') as file:
		mycsv = csv.reader(file)
		for row in mycsv:
			text = row[row_number].split(', ')
			mylist.append(text[0].split(' (')[0])

	return mylist

def make_final_list(mylist):
	myset = set(mylist)
	count_list = []

	for name in myset:
		count_list.append(count(name, mylist))

	return list(zip(myset, count_list))

def get_actors(input_file, output_file):
	actors = make_actors_list_from(input_file, 2)
	actors.remove('Stars')
	final_list = make_final_list(actors)
	write_csv(output_file, final_list)

def get_directors(input_file, output_file):
	directors = make_list_from(input_file, 3)
	directors.remove('Director')
	final_list = make_final_list(directors)
	write_csv(output_file, final_list)

def get_writers(input_file, output_file):
	writers = make_writer_list_from(input_file, 4)
	writers.remove('Writers')
	final_list = make_final_list(writers)
	write_csv(output_file, final_list)

def get_genres(input_file, output_file):
	genres = make_list_from(input_file, 5)
	genres.remove('Genres')
	final_list = make_final_list(genres)
	write_csv(output_file, final_list)

def get_company(input_file, output_file):
	company = make_list_from(input_file, 9)
	company.remove('Production Company')
	final_list = make_final_list(company)
	write_csv(output_file, final_list)


if __name__ == '__main__':
	get_actors(argv[1], argv[2])
	get_directors(argv[1], argv[3])
	get_writers(argv[1], argv[4])
	get_genres(argv[1], argv[5])
	get_company(argv[1], argv[6])